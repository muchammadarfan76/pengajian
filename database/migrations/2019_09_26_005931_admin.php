<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Admin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama');
            $table->string('email')->unique();
            $table->string('jenis_kelamin');
            $table->string('alamat');
            $table->string('tempat_lahir');
            $table->date('tgl_lahir');
            $table->string('no_hp')->nullable();
            $table->string('photo')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('type');
            $table->rememberToken();
            $table->timestamps();
        });

        DB::table('admins')->insert(
            array(
                'nama' => 'admin',
                'email' => 'kamulyan1996@gmail.com',
                'jenis_kelamin' => 'L',
                'alamat' => 'JL.Duren sawit',
                'tempat_lahir' => 'test',
                'tgl_lahir' => '2019-08-20',
                'email_verified_at' => '2019-08-20 11:50:37',
                'password' => Hash::make('admin123'),
                'type' => 'guru'
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
