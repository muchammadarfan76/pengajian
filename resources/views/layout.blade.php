<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>KBM Pondok Benda Indah</title>
    <link href="{{ url('/js/plugins/nucleo/css/nucleo.css') }}" rel="stylesheet" />
    <link href="{{ url('/css/fontawesome/css/all.min.css') }}" rel="stylesheet" />
    <link href="{{ url('/css/argon-dashboard.min.css') }}" rel="stylesheet" />
    <link href="{{ url('/css/optional.css') }}" rel="stylesheet" />
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
</head>

<body>
    @include('includes.sidebar')
    <div class="main-content">
        @include('includes.header')
        <div class="container-fluid mt--7">
            @yield('content')
            @include('includes.footer')
        </div>
    </div>
</body>
<script src="{{ url('/js/plugins/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ url('/js/plugins/chart.js/dist/Chart.min.js') }}"></script>
<script src="{{ url('/js/plugins/chart.js/dist/Chart.extension.js') }}"></script>
<script src="{{ url('/js/argon-dashboard.min.js?v=1.1.0') }}"></script>
<script src="https://cdn.trackjs.com/agent/v3/latest/t.js"></script>
<script>
    window.TrackJS &&
        TrackJS.install({
            token: "ee6fab19c5a04ac1a32a645abde4613a",
            application: "argon-dashboard-free"
        });

</script>

</html>
