<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>
        KBM Pondok Benda Indah
    </title>
    <link href="../assets/img/brand/favicon.png" rel="icon" type="image/png">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link href="{{ url('/js/plugins/nucleo/css/nucleo.css') }}" rel="stylesheet" />
    <link href="{{ url('/css/fontawesome/css/all.min.css') }}" rel="stylesheet" />
    <link href="{{ url('/css/argon-dashboard.css?v=1.1.0') }}" rel="stylesheet" />
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="{{ url('/js/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>

    <script>
        $(document).ready(function () {
            var d = new Date();
            document.getElementById('year').innerHTML = '© ' + d.getFullYear();
        });

    </script>
</head>

<body class="bg-default">
    <div class="main-content">
        <!-- Navbar -->
        <nav class="navbar navbar-top navbar-horizontal navbar-expand-md navbar-dark">
            <div class="container px-4">
                <a class="navbar-brand" href="../index.html">
                    <img src="{{ url('/images/quran.png') }}" />
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-collapse-main"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbar-collapse-main">
                    <!-- Collapse header -->
                    <div class="navbar-collapse-header d-md-none">
                        <div class="row">
                            <div class="col-6 collapse-brand">
                                <a href="../index.html">
                                    <img src="{{ url('/images/quran.png') }}">
                                </a>
                            </div>
                            <div class="col-6 collapse-close">
                                <button type="button" class="navbar-toggler" data-toggle="collapse"
                                    data-target="#navbar-collapse-main" aria-controls="sidenav-main"
                                    aria-expanded="false" aria-label="Toggle sidenav">
                                    <span></span>
                                    <span></span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <!-- Navbar items -->
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link nav-link-icon" href="../index.html">
                                <i class="fas fa-home"></i>
                                <span class="nav-link-inner--text">Home</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-link-icon" href="../examples/register.html">
                                <i class="ni ni-circle-08"></i>
                                <span class="nav-link-inner--text">Register</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-link-icon" href="../examples/login.html">
                                <i class="ni ni-key-25"></i>
                                <span class="nav-link-inner--text">Login</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-link-icon" href="../examples/profile.html">
                                <i class="ni ni-single-02"></i>
                                <span class="nav-link-inner--text">Profile</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Header -->
        <div class="header bg-gradient-primary py-7 py-lg-8">
            <div class="container">
                <div class="header-body text-center mb-7">
                    <div class="row justify-content-center">
                        <div class="col-lg-5 col-md-6">
                            <h1 class="text-white">Selamat datang!</h1>
                            <p class="text-lead text-light">Silahkan daftar untuk dapat mengakses sistem KBM Pondok
                                Benda
                                Indah</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="separator separator-bottom separator-skew zindex-100">
                <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1"
                    xmlns="http://www.w3.org/2000/svg">
                    <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
                </svg>
            </div>
        </div>
        <!-- Page content -->
        <div class="container mt--8 pb-5">
            <div class="row justify-content-center">
                <div class="col-lg-5 col-md-7">
                    <div class="card bg-secondary shadow border-0">
                        <div class="card-body px-lg-5 py-lg-5">
                            <div class="text-center text-muted mb-4">
                                <h1 class="text-black">Pendaftaran</h1>
                            </div>
                            <form role="form" action="{{ route('register') }}" method="POST">
                                @csrf
                                <div class="form-group mb-3 @error('nama')has-danger @enderror">
                                    <div class="input-group input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-user"></i></span>
                                        </div>
                                        <input class="form-control @error('nama')is-invalid @enderror"
                                            placeholder="Nama lengkap" type="text" id="nama" name="nama" required>
                                        @error('nama')
                                        <span class="invalid-feedback text-center" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group mb-3 @error('email')has-danger @enderror">
                                    <div class="input-group input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                                        </div>
                                        <input class="form-control @error('email')is-invalid @enderror"
                                            placeholder="Email" type="email" id="email" name="email" required>
                                        @error('email')
                                        <span class="invalid-feedback text-center" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group mb-3">
                                    <label for="" class="text-muted">
                                        Jenis kelamin</label>
                                    <div class="row">
                                        <div class="col">
                                            <div class="custom-control custom-radio mb-3">
                                                <input name="jenis_kelamin" class="custom-control-input" id="laki"
                                                    type="radio" value="L" checked required>
                                                <label class="custom-control-label" for="laki">Laki-laki</label>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div style="float:left;">
                                                <div class="custom-control custom-radio mb-3">
                                                    <input name="jenis_kelamin" class="custom-control-input"
                                                        id="perempuan" value="P" type="radio">
                                                    <label class="custom-control-label"
                                                        for="perempuan">Perempuan</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group @error('alamat')has-danger @enderror">
                                    <textarea name="alamat" id="alamat" cols="30" rows="10" placeholder="Alamat"
                                        class="form-control form-control-alternative @error('alamat')is-invalid @enderror"
                                        required></textarea>
                                    @error('alamat')
                                    <span class="invalid-feedback text-center" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group @error('tempat')has-danger @enderror">
                                    <div class="input-group input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-map-marker-alt"></i></span>
                                        </div>
                                        <input class="form-control @error('tempat')is-invalid @enderror"
                                            placeholder="Tempat lahir" type="text" id="tempat_lahir" name="tempat_lahir"
                                            required>
                                        @error('tempat')
                                        <span class="invalid-feedback text-center" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group @error('tanggal')has-danger @enderror">
                                    <div class="input-group input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-calendar-grid-58"></i></span>
                                        </div>
                                        <input class="form-control datepicker" id="tgl_lahir" name="tgl_lahir"
                                            placeholder="Tanggal lahir" autocomplete="false" type="text" required data-date-format="yyyy-mm-dd">
                                    </div>
                                </div>
                                <div class="form-group @error('no_hp')has-danger @enderror">
                                    <div class="input-group input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fas fa-mobile-alt"></i></span>
                                        </div>
                                        <input class="form-control @error('no_hp')is-invalid @enderror"
                                            placeholder="No.Hp" type="number" id="no_hp" name="no_hp" required>
                                        @error('no_hp')
                                        <span class="invalid-feedback text-center" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group @error('password')has-danger @enderror">
                                    <div class="input-group input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                        </div>
                                        <input class="form-control @error('password')is-invalid @enderror"
                                            placeholder="Password" autocomplete="new-password" type="password"
                                            id="password" name="password" required>
                                        @error('password')
                                        <span class="invalid-feedback text-center" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                        </div>
                                        <input class="form-control" autocomplete="new-password"
                                            placeholder="Konfirmasi password" type="password" id="password_confirm"
                                            name="password_confirmation" required>
                                    </div>
                                </div>
                                <div class="form-group mb-3">
                                    <label for="" class="text-muted">
                                        Mendaftar sebagai</label>
                                    <div class="row">
                                        <div class="col">
                                            <div class="custom-control custom-radio mb-3">
                                                <input name="type" class="custom-control-input" id="murid" type="radio"
                                                    value="murid" checked required>
                                                <label class="custom-control-label" for="murid">Murid</label>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div style="float:left;">
                                                <div class="custom-control custom-radio mb-3">
                                                    <input name="type" class="custom-control-input" id="guru"
                                                        value="guru" type="radio">
                                                    <label class="custom-control-label" for="guru">Guru</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary my-4">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-6">
                            <a href="#" class="text-light"><small>Forgot password?</small></a>
                        </div>
                        <div class="col-6 text-right">
                            <a href="#" class="text-light"><small>Login</small></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer class="py-5">
            <div class="container">
                <div class="row align-items-center justify-content-xl-between">
                    <div class="col-xl-6">
                        <div class="copyright text-center text-xl-left text-muted">
                            <label id="year">© 2019</label> <a href="https://www.creative-tim.com"
                                class="font-weight-bold ml-1" target="_blank">KBM Pondok benda indah</a>
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <ul class="nav nav-footer justify-content-center justify-content-xl-end">
                            <li class="nav-item">
                                <a href="https://www.creative-tim.com" class="nav-link" target="_blank">Tentang</a>
                            </li>
                            <li class="nav-item">
                                <a href="https://www.creative-tim.com/presentation" class="nav-link"
                                    target="_blank">Kontak</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <script src="{{ url('/js/plugins/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ url('/js/argon-dashboard.min.js?v=1.1.0') }}"></script>
    <script src="https://cdn.trackjs.com/agent/v3/latest/t.js"></script>
    <script>
        window.TrackJS &&
            TrackJS.install({
                token: "ee6fab19c5a04ac1a32a645abde4613a",
                application: "argon-dashboard-free"
            });

    </script>
</body>

</html>
