<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware'=> 'auth'], function(){
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
    Route::get('/absen', 'AbsenController@index')->name('absen');
    Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');
});

Route::prefix('admin')->group(function(){
    Route::get('/login', 'AuthAdmin\LoginController@showLoginForm');
    Route::post('/login', 'AuthAdmin\LoginController@login')->name('adminLogin');
    Route::get('/logout', 'AuthAdmin\LoginController@logout')->name('admin.logout');
    Route::get('/dashboard', 'AdminController@index')->name('admin.dashboard');
});

//sementara blm middleware karena login dan daftar blm work
Route::get('/informasi');
Route::get('/galeri');
Route::get('/data-diri');
Route::get('/jadwal');
Route::get('/raport');
